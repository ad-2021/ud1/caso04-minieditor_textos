package es.batoi.caso04_minieditor;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author sergio
 */
public class AppController implements Initializable {

    @FXML
    private TextArea taEditor;
    @FXML
    private Label lblInfo;
    @FXML
    private Button btnAbrir;
    @FXML
    private Button btnCerrar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnNuevo;

    private Stage escenario;    
    private File f;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//		taEditor.textProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValor, String newValor) {
//				System.out.println("Texto cambiado");
//			}
//		});
    }

    @FXML
    private void handleNuevo() { 
//    	taEditor.setDisable(false);
    }

    @FXML
    private void handleAbrir() {  
    	FileChooser fc = new FileChooser();
        fc.setTitle("ABRIR ARCHIVO");
        f = fc.showOpenDialog(escenario);
        // ...
    }

    @FXML
    private void handleGuardar() { 
    	FileChooser fc = new FileChooser();
        fc.setTitle("GUARDAR ARCHIVO");
        f = fc.showSaveDialog(escenario);
        // ...
        
    }

    @FXML
    private void handleCerrar() {
    }
   

    void setEscenario(Stage escenario) {
        this.escenario = escenario;
    }

   
}
